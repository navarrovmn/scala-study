package lectures.part2oop

object AnonymousFunction extends App {
  abstract class Animal {
    def eat: Unit
  }

  val funnyAnimal: Animal = new Animal {
    override def eat: Unit = println("hahahaaha")
  }

  println(funnyAnimal.getClass)
  funnyAnimal.eat

  class Person(name: String) {
    def sayHi: Unit = println(s"Hi, my name is $name")
  }

  val jim = new Person("Jim") {
    override def sayHi: Unit = println("My name is Jim, how can I help")
  }

  jim.sayHi

  // Anonymous classes works with traits or class (abstract or not)
}
