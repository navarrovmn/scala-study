package lectures.part2oop

import playground.{Cinderela => Pri, PrinceCharming}

object PackagesAndImports extends App {
  // Can access package members with their simple name
  val writer = new Writer("Daniel", "RockTheJVM", 2019)
  val princess = new Pri
  val prince = new PrinceCharming

//  packages are in hierarchy
//  matching folder structure
//  package object

//  if you need to use same class name from different packages, use alias above
//  or use fully qualified name
}
