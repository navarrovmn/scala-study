package lectures.part2oop

object InheritanceAndTraits extends App {
  sealed class Animal { // This class can only be subtyped in this file
    val creatureType = "wild"
//    can be private (only this class can use) or protected (only subclasses can access this)
    def eat = println("nomnom")
  }

  class Cat extends Animal

  class Dog(override val creatureType: String) extends Animal {
    override def eat = println("crunchcrunch")
  }

  val cat = new Cat
  cat.eat

  val dog = new Dog("domestic")
  dog.eat
  println(dog.creatureType)

  class Person(name: String, age: Int)
  class Adult(name: String, age: Int, idCard: String) extends Person(name, age)
}
