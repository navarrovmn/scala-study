package lectures.part2oop

object OOBasics extends App {
//  val person = new Person("Victor", 24)
//  println(person.age)
//  println(person.x)
//  person.greet("Daniel")

  val author = new Writer("Victor", "Navarro", 1996)
  val imposter = new Writer("Victor", "Navarro", 1996)
  val novel = new Novel("Great Expectations", 2016, author)

  println(novel.authorAge)
  println(novel.isWrittenBy(imposter))
}

class Writer(val firstName: String, val surname: String, val year: Int) {
  def fullname: String = s"$firstName $surname"
}

class Novel(val name: String, val released: Int, val author: Writer) {
  def authorAge: Int = released - author.year
  def isWrittenBy(author: Writer): Boolean = this.author == author
  def copy(newRelease: Int): Novel = new Novel(name, newRelease, author)
}

class Counter(val count: Int) {
  def currentCount: Int = count
  def inc: Counter = new Counter(currentCount + 1)
  def dec: Counter = new Counter(currentCount - 1)
  def inc(amount: Int): Counter = new Counter(currentCount + amount)
  def dec(amount: Int): Counter = new Counter(currentCount - amount)
}

// class parameters are NOT FIELDS
// to transform into fields, pass an val
class Person(name: String, val age: Int = 0) { // constructor
  val x = 2
  println(1 + 3)

  def greet(name: String): Unit = println(s"${this.name} says: hi, $name")
  def greet(): Unit = println(s"Hi, I am $name") // overloading

  // multiple constructors
  def this(name: String) = this(name, 0)
  def this() = this("John Doe")
}