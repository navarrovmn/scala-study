package lectures.part2oop

object Generics extends App {
  class MyList[+A] {
    // We would be able to add a dog in a list of cats, because dog is a animal which is >: Cat. Then, return list of Animals instead of list of cats
    def add[B >: A](element: B): MyList[B] = ???
  }

  val listOfIntegers = new MyList[Int]
  val listOfStrings = new MyList[String]

  //  generic methods
  object MyList {
    def empty[A]: MyList[A] = ???
  }

  val emptyListOfInt = MyList.empty[Int]

  class Animal
  class Cat extends Animal
  class Dog extends Animal

//  Variance problem
//  1. yes List[Cat] extends List[Animal] = COVARIANCE
  class CovariantList[+A]
  val animal: Animal = new Cat
  val animalList: CovariantList[Animal] = new CovariantList[Cat]

//  2. no = INVARIANCE
  class InvariantList[A]
//  val invariantAnimalList: InvariantList[Animal] = new InvariantList[Cat]

//  3. hell, no! CONTRAVARIANCE (not intuitive for lists. but think about an animal trainer)
  class Trainer[-A]
  val contraVariantList: Trainer[Cat] = new Trainer[Animal]

//  bounded types
  class Cage[A <: Animal](animal: A) // only accepts subtypes of animal
//  val cage = new Cage[Dog](new Dog)
  val cage = new Cage(new Dog)


}
