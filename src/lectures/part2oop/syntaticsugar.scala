package lectures.part2oop

object syntaticsugar extends App {
  class Person(val name: String, favoriteMovie: String, val age: Int = 0) {
    def likes(movie: String): Boolean = movie == favoriteMovie
    def +(person: Person): String = s"${this.name} is hanging out with ${person.name}"
    def unary_! : String = s"$name, what the heck?"
    def isAlive: Boolean = true
    def apply(): String = s"Hi, my name is $name, and I like $favoriteMovie"

//    Exercises
    def +(aka: String): Person = new Person(s"$name ($aka)", favoriteMovie)
    def unary_+ : Person = new Person(name, favoriteMovie, age + 1)
    def apply(times: Int): String = s"$name watched $favoriteMovie $times times"
    def learns(subject: String) = s"$name learns $subject"
    def learnsScala: String = learns("Scala")
  }

  val mary = new Person("Mary", "Inception")
//  println(mary.likes("Inception"))
//  println(mary likes "Inception")

  val tom = new Person("Tom", "Fight Club")
//  println(mary + tom)
//
//  println(!mary)
//  println(mary.unary_!)
//  println(mary.isAlive)
//  println(mary isAlive)
//  println(mary.apply())
//  println(mary())
  println((mary + "the rockstar").name)
  println((+mary).age)
  println(mary(2))
  println(mary learnsScala)
}
