package lectures.part2oop

object AbstractDataTypes extends App {
  abstract class Animal {
    val creatureType: String
    def eat: Unit
  }

  class Dog extends Animal {
    override val creatureType: String = "Canine"
    override def eat: Unit = println("Crunch, crunch")
  }

  trait Carnivore {
    def eat(animal :Animal): Unit
  }

  class Crocodile extends Animal with Carnivore {
    override val creatureType: String = "Croc"

    override def eat: Unit = println("nomnomnom")
    override def eat(animal: Animal): Unit = print(s"Im a croc and I'm eating a ${animal.creatureType}")
  }

  val dog = new Dog
  val croc = new Crocodile

  croc.eat(dog)

//  traits vs abstract classes
//  1 - traits do not have constructor params
//  2 - multiple traits may be inherited the same class
//  3 - represents behaviours
}
