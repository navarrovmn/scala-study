package lectures.part2oop

object Object extends App {
  object Person {
    val N_EYES = 2
    def canFly: Boolean = false
  }
  class Person {
    //    Companion objects
  }

  println(Person.N_EYES)
  println(Person.canFly)

  val mery = Person
  val john = Person

  println(mery == john)
}
