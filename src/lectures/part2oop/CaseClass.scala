package lectures.part2oop

object CaseClass extends App {
  case class Person(name: String, age: Int)

//  1. class parameters are promoted to fields
  val jim = new Person("Jim", 34)
  println(jim.age)
//  2. sensible toString. Also, println(instance) = println(instance.toString)
  println(jim.toString)
//  3. equals and hashcode implemented OOTB
  val jim2 = new Person("Jim", 34)
  println(jim == jim2)
//  4. CCs have handy copy method
  val jim3 = jim.copy(age = 45)
  println(jim3.age)
//  5. CCs have companion objects
  val thePerson = Person
  println(thePerson)
  val mary = Person("Mary", 34)
//  6. CCs are serializable
//  Akka
//  7. CCs have extractor patterns = can be used in pattern match

  sayHello
  SPEED_OF_LIGHT
}
