package lectures.fp

object CurriedFunctions extends App {
  def nTimes(f: Int => Int, n: Int, x: Int): Int =
    if(n <= 0) x
    else nTimes(f, n - 1, f(x))

  val plusOne = (x: Int) => x + 1
  println(nTimes(plusOne, 10, 1))

  //  Look again later
  def nTimesBetter(f: Int => Int, n: Int): (Int => Int) = {
    if(n <= 0) (x: Int) => x
    else (x: Int) => nTimesBetter(f, n-1)(f(x))
  }

  val plus10 = nTimesBetter(plusOne, 10)
  println(plus10(1))

  // functions with multiple parameter lists
  def curriedFormater(c: String)(x: Double): String = c.format(x)

  val standardFormat: (Double => String) = curriedFormater("%4.2f")
  val preciseFormat: (Double => String) = curriedFormater("%10.8f")

  println(standardFormat(Math.PI))
  println(preciseFormat(Math.PI))

  def toCurry(f: (Int, Int) => Int): Int => (Int => Int) =
    (x: Int) => (y: Int) => f(x, y)

  def fromCurry(f: (Int => Int => Int)): (Int, Int) => Int =
    (x: Int, y: Int) => f(x)(y)

  def compose[A, B, T](f: A => B, g: T => A): T => B =
    x => f(g(x))

  def andThen[A, B, T](f: A => B, g: B => T): A => T =
    x => g(f(x))

  def anotherAdder: Int => Int => Int = toCurry(_ + _)
  def add4 = anotherAdder(4)
  println(add4(17))

  def simpleAdder: (Int, Int) => Int = fromCurry(anotherAdder)
  println(simpleAdder(10, 10))

  val add2 = (x: Int) => x + 2
  val times3 = (x: Int) => x * 3

  val composed = compose(add2, times3)
  val ordered = andThen(add2, times3)

  println(composed(10)) // should be 32
  println(ordered(10)) // should be 36
}
