package lectures.fp

object StandardLibrary extends App{
  val list = List(1, 2, 3)
  println(list)

//  map
  println(list.map(_ * 10))
  println(list.filter(_ % 2 != 0))
  println(list.flatMap(x => List(x, x * 2, x * 3)))

  val numbers = List(1, 2, 3, 4)
  val chars = List('a', 'b', 'c', 'd')

  // A double loop is a flatmap and a map. Also can expand to other, like 3 loops being a flatmap flatmap map
  println(chars.flatMap(ch => numbers.map(y => s"$ch$y")))

  // foreach
  list.foreach(println)

  // for-comprehensions

  val forCombinations = for {
    n <- numbers if n % 2 != 0
    c <- chars
  } yield "" + n + c

  println(forCombinations)

  abstract class Maybe[+T] {
    def map[B](f: T => B): Maybe[B]
    def flatMap[B](f: T => Maybe[B]): Maybe[B]
    def filter(f: T => Boolean): Maybe[T]
  }

  case object MaybeNot extends Maybe[Nothing] {
    override def map[B](f: Nothing => B): Maybe[B] = MaybeNot
    override def flatMap[B](f: Nothing => Maybe[B]): Maybe[B] = MaybeNot
    override def filter(f: Nothing => Boolean): Maybe[Nothing] = MaybeNot
  }

  case class Just[T](value: T) extends Maybe[T] {
    override def map[B](f: T => B): Maybe[B] = Just(f(value))
    override def flatMap[B](f: T => Maybe[B]): Maybe[B] = f(value)
    override def filter(f: T => Boolean): Maybe[T] =
      if(f(value)) this
      else MaybeNot
  }

  val just3 = Just(3)
  println(just3)
  println(just3.map(_ * 10))
  println(just3.flatMap(_ => Just(10)))
  println(just3.filter(_ % 2 == 0))
}
