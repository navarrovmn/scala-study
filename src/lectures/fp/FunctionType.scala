package lectures.fp

object FunctionType extends App {
  // This is the same as val adder: Function2[Int, Int, Int]
  val adder: ((Int, Int) => Int) = new Function2[Int, Int, Int] {
    override def apply(x: Int, y: Int): Int = x + y
  }
  // All functions are objects!
  val concatStrings: ((String, String) => String) = new Function2[String, String, String] {
    override def apply(s1: String, s2: String): String = s1 + s2
  }

  // Function
  val returnFunction: (Int => Int => Int) = new Function1[Int, Function1[Int, Int]] {
    override def apply(x: Int) : (Int => Int) = {
      new Function1[Int, Int] {
        override def apply(y: Int): Int = x + y
      }
    }
  }

  println(returnFunction(10)(10)) // curried function
  println(concatStrings("Hello ", "World"))
}
