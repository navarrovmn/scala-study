package lectures.fp

object AnonymousFunction extends App {
    val tenfold = new Function1[Int, Int] {
      override def apply(v1: Int): Int = v1 * 100
    }

    // Anonymous function or lambda
    val doubler: Int => Int = (x: Int) => x * 2
    // No params
    val justPrint: () => Unit = () => println("Surprise!")

    // This will print an instance of anonymous function (you didn't actually called it, did you?)
    println(justPrint)
    justPrint()
    println(tenfold(10))
    println(doubler(2))

    // curly braces
    val stringToInt = { (s1: String) =>
        s1.toInt
    }
    println(stringToInt("10"))

    // more syntax sugar for anonymous function
  val niceIncrementer: Int => Int = _ + 1 // equivalent to x => x + 1
  val niceAdder: (Int, Int) => Int = _ + _ // equivalent to (a, b) => a + b

  println(niceIncrementer(1))
  println(niceAdder(2, 3))

//  // Function
//  val returnFunction: (Int => Int => Int) = new Function1[Int, Function1[Int, Int]] {
//    override def apply(x: Int) : (Int => Int) = {
//      new Function1[Int, Int] {
//        override def apply(y: Int): Int = x + y
//      }
//    }
//  }
  val specialAdder: Int => Int => Int = (a: Int) => (b: Int) => a + b
  println(specialAdder(1)(2))
 }
