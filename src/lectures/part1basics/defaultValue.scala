package lectures.part1basics

import scala.annotation.tailrec

object defaultValue extends App {
  @tailrec
  def trFact(n: Int, acc: Int = 1): Int = {
    if(n <= 1) acc
    else trFact(n-1, n*acc)
  }

  val fact10 = trFact(10, 2)
  val fact3 = trFact(3)

  def savePicture(width: Int = 1920, height: Int = 1080): Unit =
    println("Saving picture")


  savePicture()
  savePicture(height = 300, width = 200)
  println(fact10)
  println(fact3)
}
