package lectures.part1basics

object cbnvscbv extends App {
  def callByValue(x: Long): Unit = {
    println("by value: " + x)
    println("by value: " + x)
  }

  def callByName(x: => Long): Unit = {
    println("by name: " + x)
    println("by name: " + x)
  }

  callByValue(System.nanoTime())
  callByName(System.nanoTime())

  //  Example to show that in call-by-name, values are not evaluated until used
  //  Function below is executed infinitely, but ONLY if called
  def infinite(): Int = 1 + infinite()
  def printFirst(x: Int, y: => Int) = println(x)

//  printFirst(infinite(), 34);
  printFirst(34, infinite());
}
