package lectures.part1basics

import scala.annotation.tailrec

object functions extends App {
  def parameterLessFunction(): Int = 42
  def anotherParameterlessFunction: Int = 42

  // Use recursion instead of loops
  def aRepeatedFunction(aString: String, n: Int): Unit = {
    println(aString)
    if(n > 1)
      aRepeatedFunction(aString, n - 1)
  }

  def isPrime(n: Int): Boolean = {
    def isPrimeUntil(t: Int): Boolean =
      if(t <= 1) true
      else n % t != 0 && isPrimeUntil(t - 1)

    isPrimeUntil(n/2)
  }

  def isPrimeTail(n: Int): Boolean = {
    @tailrec
    def primeHelperTail(t: Int, stillPrime: Boolean): Boolean = {
      if(!stillPrime) false
      else if(t <= 1) true
      else primeHelperTail(t-1, n % t != 0)
    }

    primeHelperTail(n / 2, n % (n / 2) != 0)
  }

//  println(parameterLessFunction()) // This can be called either way
//  println(parameterLessFunction)
//  println(anotherParameterlessFunction) // This can't be called with parenthesis because it was defined without it
//  aRepeatedFunction("aha", 5)

//  println(isPrime(37))
//  println(isPrime(2003))
//  println(isPrime(37 * 17))
  println(isPrimeTail(37))
  println(isPrimeTail(2003))
  println(isPrimeTail(37 * 17))
}
